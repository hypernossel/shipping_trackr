<?php
require("shipping_tracker.php");
session_start();

$logged_in = false;
$tracker = new ShippingTracker();

if (isset($_POST['btn-login'])) {
    $user = $_POST['username'];
    $password = hash("sha256", md5($_POST['password']));
    $res = $tracker->checkUserLogin($user,$password);
    if ($res != -1) {
        $logged_in = true;
        $_SESSION['page'] = "mod";
        $_SESSION['uid'] = $res;
        $_SESSION['session'] = base64_encode(strtotime("now"));
    }
}
if (isset($_SESSION['session'])) {
    $last_logged_in_timestamp = base64_decode($_SESSION['session']);
    if ((strtotime("now") - $last_logged_in_timestamp) >= 600) {
        $_SESSION['page'] = "index";
        $_SESSION['session'] = "";
        session_destroy();
    }
}else{
    $_SESSION['page'] = "index";
}

if (isset($_GET['page'])) {
    if (is_null($_GET['page']) || $_GET['page'] == "")
        return;

    if(!isset($_SESSION['session']))
        $_SESSION['page'] = 'index';
    else
        $_SESSION['page'] = $_GET['page'];
}

// Same page API's
if (isset($_POST['method'])) {
    header('Content-type: application/json');
    if ($_POST['method'] == "add_tracking") {
        $required_field = ["user", "tracking", "courier","title"];
        foreach ($required_field as $field) {
            if (!isset($_POST["$field"]))
                output(json_encode(array("success" => false, "debug_msg" => "Field $field is Required")));
        }
        $res = $tracker->add_tracking($_POST['user'], $_POST['courier'],$_POST['tracking'],$_POST['title'],$_POST['tags']);
        return output(json_encode(array("success" => $res)));
    } else if ($_POST['method'] == "get_tracking_no") {
        if (!isset($_SESSION['uid']))
            output(json_encode(array("success" => false, "debug_msg" => "Required Field User")));
        return $tracker->getTrackingNoList($_SESSION['uid']);
    } else if ($_POST['method'] == "update_tracking_status") {
        if (!isset($_POST['tracking']))
            output(json_encode(array("success" => false, "debug_msg" => "Required Field Tracking Status")));
        $result = $tracker->setTrackingRecieved($_POST['tracking']);
        output(json_encode(array("success"=>(($result != 200) ? false : true), "debug_msg"=>$tracker->getTrackingErrorMsg($result))));
    } else if ($_POST['method'] == "get_tracking_info") {
        if (!isset($_POST['tracking'])) {
            output(json_encode(array("success" => false, "debug_msg" => "Required Field Tracking No")));
        }

        $tracking_info = $tracker->getTrackingInformation($_POST['tracking']);
        if ($tracking_info == 202)
            output(json_encode(array("success" => false, "debug_msg" => $tracker->getTrackingErrorMsg(202))));

        output(json_encode(array("success" => true, "data" => $tracker->getTrackingInformation($_POST['tracking']))));
    }else if($_POST['method'] == "get_tracking_status"){
        if (!isset($_POST['tracking']))
            output(json_encode(array("success" => false, "debug_msg" => "Required Field Tracking No")));

        $status = $tracker->getTrackingStatus($_POST['tracking']);
        output(json_encode(array("success"=>true,"tracking_status"=>$status,"debug_msg"=>$tracker->TRACKING_STATUS_MSG[$status])));
    }
}

function output($data)
{
    echo $data;
    exit(0);
}

?>
<html>
<head>
    <title>ShippyTrackr</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput-typeahead.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css"/>
    <link rel="stylesheet" href="font-css.css">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
            integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
            integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js"></script>
    <script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.min.js"></script>
    <style>
        body {
            color:#4363E9;
        }

        #background{
            width: 100%;
            height:70%;
            background-image: url('resource/HP_Banner.png');
            background-size: cover;
            background-repeat: no-repeat;
        }
        #background-2{
            position:absolute;
            width: 100%;
            height:100%;
            background-image: url('resource/new_bg.png');
            background-size: cover;
            background-repeat: no-repeat;
        }

        #logo{
            position: absolute;
            top:50px;
            margin-left:20px;
        }
        #title{
            position: absolute;
            top:20px;
            right:50px;
            float:right;
            font-weight: bolder;
            color:white;
        }
        #title h5{
            font-family: "Proxima Nova";
            font-size:30px;
        }
        #title small{
            position:relative;
            font-family: "Proxima Nova Alt Thin";
            font-size: 12px;
            top:-5px;
        }

        .frm-login {
            background: rgba(255, 255, 255, 1);
            padding: 80px;
            margin-top: 20%;
            border-radius: 5px;
            box-shadow: 1px 2px 20px rgba(95,137,221,0.24);
            z-index: 999999;
        }

        .login-container{
            position: absolute;
            top:15%;
        }

        .panel-tracking {
            background: white;
            padding: 10px;
            margin-top: 10%;
            border-radius: 5px;
            box-shadow: 1px 2px 3px black;
            z-index: 999999;
        }

        .links {
            color: black;
            text-decoration: none;
        }

        .links:hover {
            color: darkgrey;
        }

        #ship_table {
            margin:0 20px 0 20px;
            max-height: 250px;
            overflow-y: scroll;
        }
        #tracker_table tr{
            border-bottom: 1px solid #E3E8FE;
        }
        .btn-complete-delivery:hover {
            background-color:green;
            cursor: pointer;
        }
        .hide { display: none;}

        /* Fix Css for Bootstrap 4*/
        .label{
            display: inline-block;
            padding: .25em .4em;
            font-size: 75%;
            font-weight: 700;
            line-height: 1;
            text-align: center;
            white-space: nowrap;
            vertical-align: baseline;
            border-radius: .25rem;
            transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
        }

        #tracking_contain { font-family: "Proxima Nova Alt Bold"}
        #label_contain {font-family: "Proxima Nova Alt Bold"}

        .theone{ font-family: "Proxima Nova Alt Bold"; color:#1A2C68;}

        .btn-add-tracking {cursor:pointer;}

        /* Fix Css for Bootstrap 4*/
        .label-info{
            color: #fff;
            background-color: #3F729B;
        }

        .bootstrap-tagsinput {
            width: 100% !important;
            border:none;
        }

        /*** Sidebar ***/
        #wrapper {
            overflow-x: hidden;
        }

        #sidebar-wrapper {
            position: absolute;
            min-height: 100vh;
            margin-left: -20rem;
            -webkit-transition: margin .25s ease-out;
            -moz-transition: margin .25s ease-out;
            -o-transition: margin .25s ease-out;
            transition: margin .25s ease-out;
            z-index: 99999;
        }

        #sidebar-wrapper .sidebar-heading {
            padding: 0.875rem 1.25rem;
            font-size: 1.2rem;
        }

        #sidebar-wrapper .list-group {
            width: 18rem;
        }

        #page-content-wrapper {
            min-width: 100vw;
        }

        #wrapper.toggled #sidebar-wrapper {
            margin-left: 0;
        }

        .btn-item {margin: 5px 20px 5px 20px;}
        .btn-item {
            background-color:#F4F6FF;
            border-color:#DFE4FC;
            color:#4363E9;
        }
        .logout-link,.contact-link{
            padding: 5px 20px 5px 20px;
        }
        .contact-link{
            position: absolute;
            bottom:10px;
        }
        .contact-link:hover{text-decoration: none;}
        .btn-item .active{
            background-color:#4363E9;
        }

        @media (min-width: 768px) {
            #sidebar-wrapper {
                margin-left: 0;
            }

            #page-content-wrapper {
                min-width: 0;
                width: 100%;
            }

            #wrapper.toggled #sidebar-wrapper {
                margin-left: -18rem;
            }
            .btn-add-tracking,.btn-complete-delivery { float:right; cursor:pointer;}
            .btn-complete-delivery {margin-top:-5px;}
        }

        @media only screen and (max-width: 600px) {
            .btn-add-tracking,.btn-complete-delivery { margin-top:10px; margin-left:-8px; float:left !important; clear: both; cursor:pointer; margin-bottom:10px;}
            hr{clear:both;}
            table{
                width: 100%;
            }

            #title{
                top:-5px;
                right:10px;
            }
        }
    </style>
</head>
<body>
<?php if (!$logged_in && $_SESSION['page'] == "index") { ?>
    <!-- Login Container -->
    <div id="background" class="full-width"></div>
    <div class="container-fluid login-container">
        <div class="row">
            <div class="col-12 col-md-4"></div>
            <div class="col-12 col-md-4">
                <div class="row">
                    <div class="col-12">
                        <div id="logo" class="">
                            <img src="resource/truck.svg" width="150" height="auto"><br>
                        </div>
                        <div id="title" class="pull-right">
                            <h5>Shipping Trackr</h5><small>Track your parcels anywhere,anytime</small>
                        </div>
                    </div>
                </div>
                <form class="frm-login" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                    <div class="row">
                        <div class="col-12">
                            <h6 style="padding-bottom:10px; font-family: 'Proxima Nova Alt Bold'; font-size:20px;" class="text-center">Login</h6>
                            <div class="form-group">
                                <label><i class="fas fa-user"></i>&emsp;Username</label>
                                <input type="text" name="username" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label><i class="fas fa-key"></i>&emsp;Password</label>
                                <input type="password" name="password" class="form-control"/>
                            </div>
                        </div>
                        <div class="col-12">
                            <button type="submit" class="btn btn-info btn-block" style="border-radius: 20px; background-color:#4363E9;" name="btn-login">Login</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-xs-12 col-md-4"></div>
        </div>
    </div>
    <!-- End of Login Container -->
<?php } else { ?>
    <?php if (isset($_SESSION['page'])) {
        $page = $_SESSION['page']; ?>
        <?php if ($page == "track") { ?>
            <!-- Sidebar -->
            <?php include("common/sidebar.common.php"); ?>
            <!-- After Login View Container -->
            <div id="background-2" class="full-width"></div>
            <!-- Tracking Container -->
            <div class="container-fluid tracking-container">
                <div class="row">
                    <div class="col-12 col-md-4"></div>
                    <div class="col-12 col-md-6">
                        <div class="panel-tracking">
                            <!-- Tracking View -->
                            <div class="content-body tracking">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="card text-white mb-12" style="">
                                            <div class="card-header" style="background-color:#E3E8FE; color:#4363E9; font-family: 'Proxima Nova Alt Bold'; font-size:15px;"><span><img src="resource/track.svg"/>&emsp;Track My Parcel</span><span class="btn-add-tracking"><img src="resource/tracking.svg" style="margin-top:-10px;"/>&emsp;Add Tracking Number</span></div>
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <label style="color:#1A2C68; font-family: 'Proxima Nova Alt Bold'">Title</label>
                                                        <div id="tracking_selection">
                                                            <select class="form-control col-xs-12 col-md-7" id="view_tracking_select">
                                                                <option value="-1">Please Select one</option>
                                                                <?php
                                                                foreach ($tracker->getTrackingNoList($_SESSION['uid']) as $tracking) {
                                                                    echo "<option value='" . $tracking['tracking_no'] . "' data-courier='" . $tracking['courier'] . "'>" . $tracking['alias'] . "</option>";
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>
                                                <p style="color:#4363E9; font-family: 'Proxima Nova Alt Bold'"><span><img src="resource/parcel.svg" />&emsp;Parcel Information</span><span class="btn-complete-delivery hide btn btn-sm btn-success"><img src="resource/mark_complete.svg" style="margin-top:-5px;"/>&emsp;Mark as Completed</span></p>
                                                <hr>
                                                <div class="row">
                                                    <div class="col-md-6 col-xs-12">
                                                        <p style="color:#1A2C68;">Tracking Number : <span id="tracking_contain">-</span></p>
                                                    </div>
                                                    <div class="col-md-6 col-xs-12">
                                                        <p style="color:#1A2C68;">Label : <span id="label_contain">-</span></p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div id="ship_table"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-2"></div>
                </div>
            </div>
            <!-- End of Tracking Container -->
        <?php } else if ($page == "mod") { ?>
            <!-- Sidebar -->
            <?php include("common/sidebar.common.php"); ?>
            <!-- After Login View Container -->
            <div id="background-2" class="full-width"></div>
            <div class="container-fluid tracking-container">
                <div class="row">
                    <div class="col-12 col-md-4"></div>
                    <div class="col-12 col-md-6">
                        <div class="panel-tracking">
                            <!-- Display Message of the Day -->
                            <div class="content-body mod">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="card text-white mb-12" style="">
                                            <div class="card-header" style="background-color:#E3E8FE; color:#4363E9; font-family: 'Proxima Nova Alt Bold'; font-size:20px;"><img src="resource/instructions.svg"/>&emsp;Instructions</div>
                                            <div class="card-body">
                                                <p style="color:#1A2C68;">Welcome to <span style="font-weight: bolder;">Shipping Trackr</span></p>
                                                <p style="color:#1A2C68; font-size:12px;">With our implemented features, you are able to find your tracking
                                                    information you have added if you have any.</p>
                                            </div>
                                        </div><br>
                                        <div class="card text-white mb-12" style="">
                                            <div class="card-header" style="background-color:#E3E8FE; color:#4363E9; font-family: 'Proxima Nova Alt Bold'; font-size:20px;"><img src="resource/Announcements.svg"/>&emsp;Announcement</div>
                                            <div class="card-body">
                                                <p style="color:#1A2C68; font-size:12px;" class="text-center">No Announcement Yet !</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-2"></div>
                </div>
            </div>
        <?php } else if($page == "signout") { ?>
            <div id="background-2" class="full-width"></div>
            <div class="container-fluid tracking-container">
                <div class="row">
                    <div class="col-12 col-md-3"></div>
                    <div class="col-12 col-md-6">
                        <div class="panel-tracking">
                            <br>
                            <!-- Display Message of the Day -->
                            <div class="content-body mod">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="alert alert-info">Signing you out Please wait
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-3"></div>
                </div>
            </div>
            <?php session_destroy(); header("Location:index.php"); ?>
    <?php } else { ?>
    <?php } ?>
        <!-- Universal Add Tracking No Modal -->
        <div class="modal fade" id="model_add_tracker" tabindex="-1" role="dialog"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="background-color:#E3E8FE;">
                        <h5 class="modal-title" style="font-size:15px;" id="exampleModalLongTitle"><img src="resource/tracking.svg" style="margin-top:-5px;">&nbsp;Add New Tracking Number</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"><img src="resource/Close.svg"></span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-12">
                                <label class="theone">Courier List</label><br>
                                <select class="form-control" name="add_track_courier">
                                    <option value="-1">Please select an option</option>
                                    <?php foreach ($tracker->getCourierList() as $courier) {
                                        echo "<option value='" . $courier['id'] . "'>" . $courier['courier_name'] . "</option>";
                                    } ?>
                                </select><br>
                            </div>
                            <div class="col-12">
                                <label class="theone">Title</label><br>
                                <input type="text" name="track_title" class="form-control" /><br>
                            </div>
                            <div class="col-12">
                                <label class="theone">Label</label>
                                <div class="input-group mb-3">
                                    <input type="text" name="add_tag" class="form-control"/>
                                    <div class="input-group-prepend">
                                        <button class="btn btn-add-tag" style="background-color:#4363E9; color:white; border-radius: 0 5px 5px 0;" type="button"><i class="fa fa-plus"></i>&nbsp;Add</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <label class="theone">My Added Labels</label><br>
                                <input type="text" class="form-control" name="my_added_tags" data-role="tagsinput">
                            </div>
                            <div class="col-12"></div>
                            <div class="col-12">
                                <label class="theone">Tracking Number</label><br>
                                <input type="text" name="add_track_tracking" class="form-control"/>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-info btn-submit-tracking" style="background-color:#4363E9;">Save</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- End of Modal -->
<?php  } ?>
<?php }?>

<script>
    $(document).ready(function () {
        $(".btn-add-tracking").click(function () {
            $("#model_add_tracker").modal();
        });

        $(".btn-complete-delivery").click(function(){
            var res = confirm("Confirm Delivery?");
            if(res){
                $.post("<?php echo $_SERVER['PHP_SELF']; ?>",{method:'update_tracking_status', tracking: $(this).attr("dat-track")},function(res){
                    if(res.success){
                        alert("Tracking Status Updated");
                        window.location.reload();
                    }else{
                        alert(res.debug_msg);
                    }
                })
            }
        });

        $("#view_tracking_select").on('change', function () {
            let tracking_val = $(this).val();
            if (tracking_val != -1) {
                $(".info-card").fadeOut();
                //Remove any existing ship_table
                $("#ship_table").empty();
                $.post("<?php echo $_SERVER['PHP_SELF']; ?>", {method: 'get_tracking_info', tracking: tracking_val},
                    function (res) {
                        if (res.success) {
                            $(".btn-complete-delivery").removeClass("hide").show().attr("dat-track",tracking_val);
                            $("#ship_table").html(res.data.ship_table);
                            $("#tracking_contain").html(tracking_val);
                            $("#label_contain").html(res.data.tags);

                            $.post("<?php echo $_SERVER['PHP_SELF']; ?>", {method:'get_tracking_status',tracking:tracking_val}, function(res){
                               if(res.success){
                                   let lbl = ""
                                   let lblbadge = "";
                                   switch(res.tracking_status){
                                       case 0:
                                           lbl = "Set Complete Delivery";
                                           lblbadge = "badge-success";
                                           break;
                                       case 1:
                                           lbl = "Information";
                                           lblbadge = "badge-info";
                                           break;
                                   }
                                   $(".btn-complete-delivery").removeClass("badge-success badge-info badge-primary").addClass(lblbadge);
                                   $(".btn-complete-delivery #status-tag").text(lbl);

                               }else{
                                   $(".btn-complete-delivery").removeClass("badge-success").addClass("badge-primary");
                                   $(".btn-complete-delivery #status-tag").text("Not Available");
                               }
                            });
                        } else {
                            $(".info-card").fadeIn();
                            $(".card-text").html(res.debug_msg);
                            $(".btn-complete-delivery").hide().addClass("hide").removeAttr('dat-track');
                        }
                    });
            }
        })

        $(".btn-add-tag").click(function(){
            let tag = $("input[name=add_tag]").val();
            $("input[name=my_added_tags]").tagsinput('add', tag);
            //clear
            $("input[name=add_tag").val("");
        });

        $(".btn-submit-tracking").click(function(){
            let tracking = $("input[name=add_track_tracking]").val();
            let courier = $("select[name=add_track_courier]").val();
            let tags = $("input[name=my_added_tags]").val();
            let title = $("input[name=track_title]").val();

            if(courier == -1) {
                alert("Invalid Courier Option"); return;
            }

            if(tracking == undefined || tracking == ""){
                alert("Invalid Tracking No."); return;
            }

            $.post("<?php echo $_SERVER['PHP_SELF']; ?>",{"method":'add_tracking', "user":"<?php echo isset($_SESSION['uid']) ? $_SESSION['uid'] : -1; ?>", tracking:tracking, courier:courier, tags:tags,title:title},function(res){
                if(res.success){
                    alert("Tracking No has been added");
                    $("input[name=add_track_tracking]").val("");
                    $("select[name=add_track_courier] option").eq(0).prop('selected',true);
                    $("#model_add_tracker").modal("hide");
                    $("input[name=my_added_tags]").tagsinput('removeAll');
                    window.location.reload();
                }else{
                    alert(res.debug_msg);
                }
            });
        });
    });
</script>
</body>
</html>
