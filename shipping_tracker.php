<?php
require "vendor/autoload.php";

class ShippingTracker{
    public $constants = array(
                        "mysql"=>array(
                            "host"=>"localhost",
                            "username"=>"thinkbox_root",
                            "password"=>"Leslie12$",
                            "dbname"=>"thinkbox_shipping"
                        ),"telegram_api_token" => "1288934922:AAE77HrqTNirOnCI2IFY3QjHmwbv22bHs5s");

    public $TRACKING_RESULT_LIST = array(
        "TRACKING_UPDATED" => 200,
        "TRACKING_ADDED" => 201,
        "TRACKING_NOT_FOUND" => 202,
        "TRACKING_ERROR" => 203,
        "DATABASE_ERROR" => 1,
        "SERVER_ERROR" => -1
    );

    public $TRACKING_STATUS_LIST = array(
      "ACTIVE" => 0,
      "COMPLETED" => 1
    );

    public $TRACKING_RESULT_MSG = array(
        200 => "Tracking Status have been updated",
        201 => "Tracking Number has been added",
        202 => "Tracking Number not found",
        203 => "Something went wrong opps.",
        -1 => "Server Error",
        1 => "Database Error"
    );

    public $TRACKING_STATUS_MSG = array(
       0 => "Status : Active",
        1 => "Status : Completed"
    );

    private $conn = null;

    public function __construct()
    {
        if(is_null($this->conn)){
            $this->conn = mysqli_connect(
                $this->constants['mysql']['host'],
                $this->constants['mysql']['username'],
                $this->constants['mysql']['password'],
                $this->constants['mysql']['dbname']);
        }
    }

    public function checkUserLogin($user,$pass){
        $users = array(
            ["pwd_hash"=> "41b89b97edf845cc042d5e76bc881d7c20dcbf596628dfbc631fbeb9ab56d686","username"=>"hyperlogin"],
            ["pwd_hash"=>"5367ea26261eff2828effd880043793b5fdac7f2e8b3a01d2906905b5e9aa429", "username"=>"jacinda"],
            ["pwd_hash"=>"0b8982ca7cc755a6aa8000c8ce3af2dfb9ce4c2c837179116c6763c1ade4be88", "username"=>"mandy"]
        );
        $i = 0;
        foreach($users as $_user){
            if($_user['username'] == strtolower($user) && $pass == $_user['pwd_hash'])
                return $i;
            $i++;
        }

        return -1;
    }

    public function getCourierList(){
        return $this->query_db("SELECT id,courier_name from courier");
    }

    public function getTrackingNoList($user){
        $query = sprintf("SELECT COALESCE(NULLIF(alias,''), 'No Title Available') as alias,tracking_no,courier from tracking_number where user=%d and status != 1",$user);
        return $this->query_db($query);
    }

    public function add_tracking($user,$courier,$tracking,$alias = "",$tags = ""){
        if($user == -1)
            return $this->TRACKING_RESULT_LIST['SERVER_ERROR'];

        if($alias == "")
            $alias = date("d-m-Y-h:s:m", strtotime("now"));

        $query = sprintf("INSERT INTO tracking_number VALUES(DEFAULT,%d,'%s','%s',%d,DEFAULT,'%s',DEFAULT )",$user,$alias,$tracking,$courier,$tags);
        return $this->query_db($query);
    }

    public function getTrackingInformation($trackingNo){
        $query = sprintf("SELECT * from tracking_number WHERE tracking_no ='%s'",$trackingNo);
        $res = $this->query_db($query);
        $rec_count = count($res);
        if($rec_count <=  0)
            return $this->TRACKING_RESULT_LIST['TRACKING_NOT_FOUND'];

        $response = json_decode($this->getTracking($trackingNo),true);
        if(is_null($response))
            return $this->TRACKING_RESULT_LIST['TRACKING_NOT_FOUND'];

        //Setup Tags
        $tags = explode(",", $res[0]["item_tag"]);

        $tagBody = "";
        for($j=0; $j < count($tags); $j++)
            $tagBody .= "$tags[$j],";

        $tblHeader = "<thead><tr style='background-color:#E3E8FE; color:#4363E9'><td>Location</td><td>Time Stamp</td></tr></thead>";
        $tblContent = "<tbody>";
        for($i = count($response) -1; $i >= 0; $i--){
            $tmp_dte = strtotime($response[$i]['time']);
            $tblContent.= "<tr><td>".$response[$i]['location']."</td><td>".date("d M Y h:m:s",$tmp_dte)."</td></tr>";
        }
        $tblContent.="</tbody>";

        $data = array(
            "ship_table" => sprintf("<table class='table' id='tracker_table' style='border-radius: 5px'>%s%s</table>",$tblHeader,$tblContent),
            "tags" => rtrim($tagBody,",")
        );
        return $data;
    }

    public function getTrackingStatus($trackingNo){
        $query = sprintf("SELECT status from tracking_number where tracking_no ='%s'",$trackingNo);
        $result = $this->query_db($query);

        if(count($result) <= 0)
            return $this->TRACKING_RESULT_LIST['TRACKING_NOT_FOUND'];

        switch($result[0][['status']]){
            case 0:
                return $this->TRACKING_STATUS_LIST['ACTIVE'];
            case 1:
                return $this->TRACKING_STATUS_LIST['COMPLETED'];
            default:
                return $this->TRACKING_RESULT_LIST['SERVER_ERROR'];
        }
    }

    public function setTrackingRecieved($tracking){
        $query = sprintf("SELECT COUNT(*) as count from tracking_number WHERE tracking_no ='%s'",$tracking);
        $rec_count = count($this->query_db($query));
        if($rec_count > 0){
            $query = sprintf("UPDATE tracking_number SET status=1 WHERE tracking_no='%s'",$tracking);
            $this->query_db($query);
            return $this->TRACKING_RESULT_LIST['TRACKING_UPDATED'];
        }else{
            return $this->TRACKING_RESULT_LIST['TRACKING_ERROR'];
        }
    }

    private function getTracking($trackingNo){
        $retrieveInfo = $this->query_db(sprintf("SELECT courier from tracking_number where tracking_no='%s'",$trackingNo));
        $query = sprintf("SELECT param_list,isAPI from courier where id=%d",$retrieveInfo[0]['courier']);
        $result = $this->query_db($query);

        $buildingList = json_decode($result[0]['param_list'],true);

        $base_url = $buildingList['app_url']."?";
        $params = "";
        //Build the Param List
        foreach($buildingList['param_list'] as $param=>$var){
            if($var === "{{tracking}}")
                $var = $trackingNo;

            $params.= $param."=".$var."&";
        }
        //Trim the extra & away
        $params = rtrim($params,"&");
        $trackinglink =  $base_url.$params;

        if($result[0]['isAPI'] == 1){
            return file_get_contents($trackinglink);
        }
    }

    private function query_db($query){
        $command = strtolower(explode(" ",$query)[0]);
        $flag = -1;
        if($command == "update" || $command == "insert" || $command == "delete")
            $flag = 0;
        else if($command == "select")
            $flag = 1;
        else {
            error_log("Invalid MySql?");
            return false;
        }
        $result = mysqli_query($this->conn,$query);

        if($flag == 0){
            if(!$result) {
                error_log("shipping_tracker.php:46 - ". mysqli_error($this->conn));
                return false;
            }else
                return true;
        }else if($flag == 1){
            return mysqli_fetch_all($result,MYSQLI_ASSOC);
        }
    }

    private function escape_string($string){
        return mysqli_real_escape_string($this->conn,$string);
    }

    public function getTrackingErrorMsg($err){
        return $this->TRACKING_RESULT_MSG[$err];
    }
}
?>
