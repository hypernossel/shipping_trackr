<!-- Sidebar -->
<div class="bg-light border-right" id="sidebar-wrapper">
    <div class="sidebar-heading"><img src="resource/image.png" width="250" height="auto" style="margin-top:20px;"></div>
    <div class="sidebar-heading text-center">How can i help you?</div>
    <div class="list-group list-group-flush">
        <a href="?page=mod" class="btn btn-primary btn-item <?php echo ($_GET['page'] == "mod") ? 'active' : ''; ?>" ><img src="resource/Home.svg" style="margin-top:-5px;">&emsp;Home</a>
        <a href="?page=track" class="btn btn-primary btn-item <?php echo ($_GET['page'] == "track") ? 'active' : ''; ?>" ><img src="resource/tracking.svg" style="margin-top:-5px;">&emsp;Track my parcel</a>
        <a href="#" class="btn btn-primary btn-item" ><img src="resource/courier.svg" style="margin-top:-5px;">&emsp;My Courier</a>
        <a href="?page=signout" class="logout-link text-center" ><img src="resource/Logout.svg" style="margin-top:-5px;">&emsp;Logout</a>
        <a href="https://t.me/hyperlogin" class="contact-link"><img src="resource/le.svg" style="margin-top:-5px;">&emsp;Contact me @hyperlogin</a>
    </div>
</div>
<!-- /#sidebar-wrapper -->