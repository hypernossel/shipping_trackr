<div class="content-head">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="?page=mod" style="font-size: 12px;padding-top:10px;"><img
                src="resource/delivery.png" width="30" height="auto"
                style="border-radius: 50%;background-color:white; object-fit: cover;">&emsp;ShippyTrackr</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse"
                data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-truck"></i>&emsp;Couriers
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <?php
                        foreach ($tracker->getCourierList() as $courier) {
                            echo "<a class=\"dropdown-item\" href=\"#\" data-attr='" . $courier['id'] . "'><i class=\"fas fa-truck-loading\"></i>&emsp;" . $courier['courier_name'] . "</a>";
                            echo "<div class=\"dropdown-divider\"></div>";
                        }
                        ?>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="?page=track"><i class="fas fa-receipt"></i>&emsp;My Tracking
                        #</a>
                </li>
            </ul>
            <ul class="navbar-nav mr-auto pull-right">
                <li class="nav-item">
                    <a class="nav-link btn btn-success btn-add-tracking" href="#"><i
                            class="fas fa-plus"></i>&emsp;Add
                        Tracking #</a>
                </li>
            </ul>
        </div>
    </nav>
</div>